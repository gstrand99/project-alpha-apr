from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm

# Create your views here.


@login_required
def show_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects, "number": len(list_projects)}

    return render(request, "projects/list.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("/projects/")
    else:
        form = CreateForm
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def detail_view(request, id):
    project = Project.objects.get(id=id)
    owner = project.owner.projects.all()
    context = {
        "detail_view": project,
        "owner": owner,
    }

    return render(request, "projects/detail.html", context)
