from django.urls import path
from projects.views import show_projects, create_project, detail_view

urlpatterns = [
    path("", show_projects, name="home"),
    path("", show_projects, name="list_projects"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/", detail_view, name="show_project"),
]
